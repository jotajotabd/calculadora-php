# Plan de Estudio sugerido para Desarrollo Web
1. Curso básico de HTML*: https://www.youtube.com/watch?v=MJkdaVFHrto
2. Curso básico de CSS*: https://www.youtube.com/watch?v=wZniZEbPAzk
3. Curso básico de JavaScript*: https://www.youtube.com/watch?v=QoC4RxNIs5M

# OJO: Los cursos son meras sugerencias, existen muchísimos recursos en la web (libros, tutoriales, cursos...)

## Ejercicio ##
Crear una calculadora que permita ejecutar las cuatro operaciones aritméticas básicas:
- Suma
- Resta
- Multiplicación
- División 

_Utilizando como herramientas o lenguajes de desarrollo HTML, CSS y JavaScript.
_No se permite usar librerías ni frameworks, todo el código será “vanilla”.
_El diseño es a libre decisión.