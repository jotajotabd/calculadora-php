
var operacion
var total = 0

/* Operadores */

const multiplicacion = document.getElementById('multiplicacion')
const division = document.getElementById('division')
const suma = document.getElementById('suma')
const resta = document.getElementById('resta')
const igual = document.getElementById('igual')
const resultado = document.getElementById('resultado')
const reset = document.getElementById('resetear')
const eliminar = document.getElementById('eliminar')
const porcentaje = document.getElementById('porcentaje')


/*__________________Eventos de CLICK NUMEROS____________________*/

function tomarOperacion(key){
    if (resultado.value === '*' || resultado.value === '/' || resultado.value === '.'){
        resultado.value = resultado.value + key
        operacion = 0
        resolver()
    }else{
        resultado.value = resultado.value + key
    }
}

/*________________Eventos de CLICK OPERADORES________________*/

igual.onclick = function(){
    operacion =  resultado.value
    resetear()
    resolver()
}

reset.onclick = function(){
    resetear()
}

eliminar.onclick = function(){
    limpiar()
}

porcentaje.onclick = function(){
    resultado.value = resultado.value / 100
}

/*________________Funciones________________*/

function limpiar(){
    resultado.value = resultado.value.slice(0, resultado.value.length - 1)
}

function resetear(){
resultado.value = ""
operador = ""
}

function resolver(){
    if(operacion === '' || operacion === '*' || operacion === '/' || operacion === '.'){
        alert('Inserte una operación matemática válida')
        resetear()
    }else{
        total = eval(operacion)
        resultado.value = total
    }
}

